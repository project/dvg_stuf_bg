# Introduction

StUF-BG is a standard used by the Dutch government. For more information about StUF-BG:
https://www.kinggemeenten.nl/secties/basis-en-kerngegevens/berichtenstandaard-stuf-bg

This module is used by the dvg_stuf_bg_tokens module for prefilling webforms using tokens.
https://www.drupal.org/project/dvg_stuf_bg_tokens

This module is currently a "beta" feature of the Drupal voor Gemeenten distribution.

# Dependencies:

* dvg_authentication_tmp_mapping (dvg)
* dvg_roles_and_permissions (dvg)
* encrypt

# Install

* This functionality is currently available in the DvG distribution as a Beta.

# Configuration

* admin/config/services/dvg-stuf-bg
