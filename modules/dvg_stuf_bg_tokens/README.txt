# Introduction

This module provides webform tokens for data provided by StUF-BG.

# Dependencies:

* dvg_stuf_bg (dvg)

# Install

* This functionality is currently available in the DvG distribution as a Beta.

# Configuration

* After configuring dvg_stuf_bg module, stuf-bg tokens will be avaialble in the token browser.